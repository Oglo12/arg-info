#![allow(dead_code)]

use std::io;
use is_terminal::IsTerminal;

pub fn being_piped() -> bool {
    return !io::stdout().is_terminal();
}

pub fn silenced_error_output() -> bool {
    return !io::stderr().is_terminal();
}
