mod data_in;
mod data_out;

use colored::Colorize;
use data_in::*;
use data_out::*;

macro_rules! print_some_string_vec {
    (
        $function: ident,
        $title: expr,
        $color: ident
    ) => {
        match $function() {
            Some(s) => {
                println!("");
                
                println!("{}", format!("{}:", $title).$color().bold());
                for i in s.iter() {
                    println!("  {}", i);
                }
            },
            None => (),
        };
    };
}

macro_rules! print_bool {
    (
        $function: ident,
        $title: expr
    ) => {
        print!("{} ", format!("{}:", $title).bright_cyan());

        match $function() {
            true => println!("{}", "TRUE".bright_green().bold()),
            false => println!("{}", "FALSE".bright_red().bold()),
        };
    };
}

fn main() {
    print_some_string_vec!(args, "Arguments", bright_green);
    print_some_string_vec!(piped, "Pipe Input", bright_blue);

    println!("");

    print_bool!(being_fed_pipe, "Piping In");
    print_bool!(being_piped, "Piping Out");
    print_bool!(silenced_error_output, "Silenced Errors");

    println!("");
}
