#![allow(dead_code)]

use std::io;
use std::env;
use is_terminal::IsTerminal;
use colored::Colorize;

pub fn args() -> Option<Vec<String>> {
    let mut env_args: Vec<String> = env::args().collect();

    if env_args.len() == 0 {
        return None;
    }

    env_args[0] = env_args[0].italic().dimmed().to_string(); // For binary path.

    return Some(env_args);
}

pub fn piped() -> Option<Vec<String>> {
    if being_fed_pipe() {
        let mut contents: Vec<String> = Vec::new();

        for i in io::stdin().lines() {
            contents.push(i.unwrap());
        }

        return Some(contents);
    }

    else {
        return None;
    }
}

pub fn being_fed_pipe() -> bool {
    return !io::stdin().is_terminal();
}
