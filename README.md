# ArgInfo

Inspect how this command is used.

Example: `ls | arg-info abc`
